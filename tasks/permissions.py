from rest_framework import permissions

from users import RoleType


class TaskPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if view.action in ['list', 'retrieve']:
            return request.user.is_authenticated
        elif view.action == 'create':
            return request.user.is_authenticated and request.user.role == RoleType.EMPLOYEE
        else:
            return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if view.action in ['update', 'partial_update', 'destroy']:
            return request.user.is_admin or obj.owner == request.user
        else:
            return True
