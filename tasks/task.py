from celery import shared_task


@shared_task
def send_message(email: str) -> None:
    print(f"Message send to {email}")