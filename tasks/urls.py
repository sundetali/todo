from django.urls import path, include
from rest_framework import routers

from tasks.views import TaskViewSet

app_name = 'tasks'

router = routers.DefaultRouter()
router.register(r'', TaskViewSet, basename='task')

urlpatterns = [
    path('', include(router.urls))
]
