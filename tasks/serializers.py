from rest_framework import serializers

from tasks import TaskPriorityTypes
from users.models import CustomUser


class TaskCreateSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=150)
    description = serializers.CharField(style={'base_template': 'text area'})
    priority = serializers.ChoiceField(choices=TaskPriorityTypes.choices, required=False)
    deadline = serializers.DateTimeField(required=False)

    def validate(self, attrs):
        if self.instance is None:
            attrs['owner'] = self.context['request'].user
        return attrs


class TaskUpdateSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=150)
    description = serializers.CharField(style={'base_template': 'text area'})
    priority = serializers.ChoiceField(choices=TaskPriorityTypes.choices, allow_null=True)
    deadline = serializers.DateTimeField(allow_null=True)
    owner = serializers.PrimaryKeyRelatedField(queryset=CustomUser.objects.all())


class TaskRetrieveSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)
    priority = serializers.CharField(read_only=True)
    deadline = serializers.DateTimeField(read_only=True)
    owner = serializers.PrimaryKeyRelatedField(read_only=True)