class TaskPriorityTypes:
    HIGH: int = 3
    MEDIUM: int = 2
    EASY: int = 1

    choices = (
        (HIGH, "HIGH"),
        (MEDIUM, "MEDIUM"),
        (EASY, "EASY"),
    )
