from typing import Optional
from datetime import datetime

from tasks.models import Task
from users.models import CustomUser


def create_task(
        title: str,
        description: str,
        owner: CustomUser,
        priority: Optional[int] = None,
        deadline: Optional[datetime] = None,
) -> Task:
    return Task.objects.create(title=title, description=description, priority=priority,
                               deadline=deadline, owner=owner)


def update_task(
        task: Task,
        title: str,
        description: str,
        priority: int,
        deadline: datetime,
        owner: CustomUser
) -> Task:
    task.title = title
    task.description = description
    task.priority = priority
    task.deadline = deadline
    task.owner = owner
    task.save()

    return task
