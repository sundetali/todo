from datetime import timedelta

from django.db import transaction
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from tasks.models import Task
from tasks.permissions import TaskPermission
from tasks.serializers import TaskCreateSerializer, TaskUpdateSerializer, TaskRetrieveSerializer
from tasks.services import create_task, update_task
from tasks.task import send_message


class TaskViewSet(ModelViewSet):
    permission_classes = (TaskPermission,)
    queryset = Task.objects.all()

    def get_serializer_class(self):
        if self.action in ['create']:
            return TaskCreateSerializer
        elif self.action in ['update']:
            return TaskUpdateSerializer
        return TaskRetrieveSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        task = create_task(**serializer.validated_data)
        if task.deadline is not None:
            deadline = task.deadline - timedelta(minutes=1)
            transaction.on_commit(lambda: send_message.apply_async((request.user.email,), eta=deadline))
        return Response(TaskRetrieveSerializer(task).data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        task = update_task(instance, **serializer.validated_data)
        return Response(TaskRetrieveSerializer(task).data)
