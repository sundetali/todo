from django.db import models

from tasks import TaskPriorityTypes


class Task(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField()
    owner = models.ForeignKey('users.CustomUser', related_name='tasks', on_delete=models.CASCADE)
    priority = models.PositiveIntegerField(choices=TaskPriorityTypes.choices, null=True, blank=True)
    deadline = models.DateTimeField(null=True, blank=True)
