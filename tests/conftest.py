import pytest
from faker import Faker
from django.urls import reverse
from rest_framework.test import APIClient

from users import RoleType
from users.models import CustomUser

TEST_EMAIL = "test@test.com"
TEST_PASSWORD = 'test'

fake = Faker()


@pytest.fixture(scope='session')
def register(django_db_setup, django_db_blocker):
    client = APIClient()
    url = reverse('users:registration')
    data = {
        'email': TEST_EMAIL,
        'first_name': fake.first_name(),
        'last_name': fake.last_name(),
        'password': fake.password()
    }
    with django_db_blocker.unblock():
        response = client.post(url, data)
        return response


@pytest.fixture(scope='session')
def login(django_db_setup, django_db_blocker):
    data = {
        'email': TEST_EMAIL,
        'password': TEST_PASSWORD
    }
    with django_db_blocker.unblock():
        user = CustomUser.objects.get(email=TEST_EMAIL)
        user.set_password(TEST_PASSWORD)
        user.save(update_fields=['password'])
        client = APIClient()
        url = reverse('users:login')
        response = client.post(url, data)
        return response


@pytest.fixture(scope='session')
def register_employee(django_db_setup, django_db_blocker):
    client = APIClient()
    url_register = reverse('users:registration')
    data = {
        'email': fake.email(),
        'first_name': fake.first_name(),
        'last_name': fake.last_name(),
        'password': fake.password(),
        'role': RoleType.EMPLOYEE
    }
    with django_db_blocker.unblock():
        response = client.post(url_register, data)
        return response


@pytest.fixture()
def register_admin(django_db_setup, django_db_blocker):
    client = APIClient()
    url_register = reverse('users:registration')
    data = {
        'email': fake.email(),
        'first_name': fake.first_name(),
        'last_name': fake.last_name(),
        'password': fake.password(),
        'is_admin': True
    }
    with django_db_blocker.unblock():
        response = client.post(url_register, data)
        return response


@pytest.fixture(scope='session')
def session(django_db_setup, django_db_blocker):
    data = {
        'email': TEST_EMAIL,
        'password': TEST_PASSWORD
    }
    with django_db_blocker.unblock():
        user = CustomUser.objects.get(email=TEST_EMAIL)
        user.set_password(TEST_PASSWORD)
        user.save(update_fields=['password'])
        client = APIClient()
        url = reverse('users:login')
        response = client.post(url, data)
        client.credentials(HTTP_AUTHORIZATION=f"Bearer {response.data.get('token', None)}")
        yield client
        client.logout()
