import pytest
from django.urls import reverse

from tasks.models import Task

NEW_TITLE = 'new title'


@pytest.fixture()
def create_task(session):
    url = reverse('tasks:task-list')
    data = {
        'title': 'test_title',
        'description': 'test_description'
    }
    response = session.post(url, data, format='json')
    return response


@pytest.fixture()
def list_task(session):
    url = reverse('tasks:task-list')
    response = session.get(url)
    return response


@pytest.fixture()
def retrieve_task(session):
    task = Task.objects.first()
    url = reverse('tasks:task-detail', kwargs={"pk": task.id})
    response = session.get(url)
    return response


@pytest.fixture()
def update_task(session):
    data = Task.objects.values().first()
    url = reverse('tasks:task-detail', kwargs={"pk": data['id']})
    data['title'] = NEW_TITLE
    data['owner'] = data.pop('owner_id')
    response = session.put(url, data, format='json')

    return response


@pytest.fixture()
def delete_task(session):
    task = Task.objects.first()
    url = reverse('tasks:task-detail', kwargs={"pk": task.id})
    response = session.delete(url)
    return response
