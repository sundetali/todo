from tests.tasks.conftest import NEW_TITLE


def test_create_task(create_task):
    assert create_task.status_code == 201


def test_list_task(list_task):
    assert list_task.status_code == 200
    assert isinstance(list_task.data, list)


def test_retrieve_task(retrieve_task):
    assert retrieve_task.status_code == 200
    assert isinstance(retrieve_task.data, dict)


def test_update_task(update_task):
    assert update_task.status_code == 200
    assert update_task.data['title'] == NEW_TITLE


def test_delete_task(delete_task):
    assert delete_task.status_code == 204
    assert not delete_task.data
