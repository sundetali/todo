from users import RoleType


def test_register(register):
    assert register.status_code == 201


def test_register_employee(register_employee):
    assert register_employee.status_code == 201
    assert register_employee.data['role'] == RoleType.EMPLOYEE


def test_register_admin(register_admin):
    assert register_admin.status_code == 201
    assert register_admin.data['is_admin']


# def test_login(login):
#     assert login.status_code == 200



